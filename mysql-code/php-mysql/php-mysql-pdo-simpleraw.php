<?php
/* Instrucciones
    ====================================================================================
    conjunto de funcionalidades para   crear y manejar bases de datos  con mysql
    ====================================================================================
*/

//========VARIABLES DE CONFIGURACION==============================================
    $driver = 'mysql';
    $database = "dbname=CODINGGROUND";
    $dsn = "$driver:host=localhost;$database";

    $username = 'root';
    $password = 'root';

    $host="localhost"; 

    $root="root"; 
    $root_password="root"; 

    $user='newuser';
    $pass='newpass';
    $db="mikeDB2";
//===================CREATE DATABASE======================================


 $sql = "CREATE DATABASE `$db`;
                CREATE USER '$user'@'localhost' IDENTIFIED BY '$pass';
                GRANT ALL ON `$db`.* TO '$user'@'localhost';
                FLUSH PRIVILEGES;";
                
 //==========================================
    try {
        $conn = new PDO("mysql:host=$host", $root, $root_password);
        //============error handling========
             //$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//finaliza la ejecicion
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //no detiene la ejecucion
       //=============================
        $conn = $conn->prepare($sql);
      $statement= $conn->execute();
        //or die(print_r($conn->errorInfo(), true));// 
       
    } catch (PDOException $e) {
        die("DB ERROR: ". $e->getMessage());
        echo "\n";
        exit;
    }
    //==========================================
    if ($statement==true) {
        echo " databse sucesfully created";
        echo "\n";
    }
//=============CONECTAR A DATA BASE============================================
    $database = "dbname=$db";
    $dsn = "$driver:host=localhost;$database";

    try {
    $conn = new PDO($dsn, $username, $password);

    }catch(PDOException $e){
    echo $e->getMessage();
    }

    if ($conn ==true)
    {
        echo "Database $db Connected\n";
        
    }

//===================CREATE TABLES =====================================

    $table= "tcompany";
    $columns = "ID INT( 11 ) AUTO_INCREMENT PRIMARY KEY, Prename VARCHAR( 50 ) NOT NULL, Name VARCHAR( 250 ) NOT NULL,
    StreetA VARCHAR( 150 ) NOT NULL, StreetB VARCHAR( 150 ) NOT NULL, StreetC VARCHAR( 150 ) NOT NULL, 
    County VARCHAR( 100 ) NOT NULL, Postcode VARCHAR( 50 ) NOT NULL, Country VARCHAR( 50 ) NOT NULL " ;

    $sql="CREATE TABLE IF NOT EXISTS $db.$table ($columns)";
    $sqlSt=$conn->prepare($sql);
    $createTable = $sqlSt->execute();

    if ($createTable) 
    {
        echo "Table $table - Created!<br /><br />";
    echo "\n";
    }
    else 
    { 
        echo "Table $table not successfully created! <br /><br />";
        echo "\n";
    }

//===============TABLE EXISTS?======================================
    /**
     * Check if a table exists in the current database.
     *
     * @param PDO $pdo PDO instance connected to a database.
     * @param string $table Table to search for.
     * @return bool TRUE if table exists, FALSE if no table found.
     */
    function tableExists($pdo, $table) {

        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        try {
            $result = $pdo->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            // We got an exception == table not found
            return FALSE;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }

    if(tableExists($conn, $table)==FALSE)
    {
        echo"no  existe la tabla \n";

    }
    else 
    {
    echo"si existe la tabla \n";  
    }

//===========SHOW TABLES=============================

    //Our SQL statement, which will select a list of tables from the current MySQL database.
    $sql = "SHOW TABLES";
    
    //Prepare our SQL statement,
    $statement = $conn->prepare($sql);
    
    //Execute the statement.
    $statement->execute();
    
    //Fetch the rows from our statement.
    $tables = $statement->fetchAll(PDO::FETCH_NUM);
    
    
    if (!empty($tables))
    {
        echo " si jay tablas \n";
    //Loop through our table names.
    foreach($tables as $table){
        //Print the table name out onto the page.
        echo $table[0], '<br>';
        echo "\n";
        
    }
    }
    else
    {
        echo "no hay tabla en la bd: $db \n";
    }

//================INSERT VALUES IN TABLE=======================

    try {
    $Name="mike";
    $StreetA="calle1";
    $Country="mx";
    $sql = "INSERT INTO tcompany ( Name, StreetA, Country) VALUES (?,?,?)";
    $stmt= $conn->prepare($sql);
    $stmt->execute([ $Name, $StreetA, $Country]);

    } catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    }

    if ($stmt==TRUE)
    {
    echo "New record created successfully";
    echo "\n";  
    }

//=============SELECT *=================================
    try {
    // select all users
    $sql = 'SELECT  * FROM tcompany';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    echo "select ejecutado\n";

    } catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    }

    if ($stmt->rowCount() > 0) {
        //Row has been found
        //Do ......
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            echo "si hay filas\n";
            foreach($row as $value)
            {
                echo sprintf("%s, ", $value);
            }
            echo "\n";
        }
    } else {
        //No row found
        //Do....
        echo "consulta sin resultados";
        echo "\n";
    }
//=======================================================
?>
