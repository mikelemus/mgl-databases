Drop function if exists FunSuma;
DELIMITER //
CREATE function FunSuma(nm1 INT, nm2 INT)
returns int
BEGIN
return nm1+nm2;
END //
delimiter ;

select FunSuma(2,3) as resultado;
